#!/bin/bash

themeName="$1"

if [ ! -f epicyon-profile.css ]; then
    echo 'Run this in your Epicyon install directory'
    exit 0
fi

if [[ "$themeName" == 'default' ]]; then
    if [ -f config.json ]; then
	if grep -q '"theme"' config.json; then
	    sed -i '/"theme"/d' config.json
	fi
    fi
    if [ -f epicyon.css ]; then
	rm epicyon.css
    fi
    if [ -f login.css ]; then
	rm login.css
    fi
    if [ -f follow.css ]; then
	rm follow.css
    fi
    if [ -f suspended.css ]; then
	rm suspended.css
    fi
    if [ -f calendar.css ]; then
	rm calendar.css
    fi
fi

if [[ "$themeName" == 'highvis'* || "$themeName" == 'highviz'* ]]; then
    if [ -f config.json ]; then
	if grep -q '"theme"' config.json; then
	    sed -i 's|"theme".*|"theme": "highvis",|g' config.json
	fi	
    fi
    filename=epicyon.css
    cp epicyon-profile.css "$filename"
    sed -i 's|--font-size-header:.*|--font-size-header: 22px;|g' "$filename"    
    sed -i 's|--font-size:.*|--font-size: 45px;|g' "$filename"
    sed -i 's|--font-size2:.*|--font-size2: 45px;|g' "$filename"
    sed -i 's|--font-size3:.*|--font-size3: 45px;|g' "$filename"
    sed -i 's|--font-size4:.*|--font-size4: 35px;|g' "$filename" 
    sed -i 's|--font-size5:.*|--font-size5: 29px;|g' "$filename" 
    filename=calendar.css
    cp "epicyon-$filename" "$filename"
    filename=login.css
    cp "epicyon-$filename" "$filename"
    sed -i 's|--font-size-header:.*|--font-size-header: 22px;|g' "$filename"    
    sed -i 's|--font-size:.*|--font-size: 45px;|g' "$filename"
    filename=follow.css
    cp "epicyon-$filename" "$filename"
    sed -i 's|--font-size-header:.*|--font-size-header: 22px;|g' "$filename"    
    sed -i 's|--font-size:.*|--font-size: 45px;|g' "$filename"
    filename=suspended.css
    cp "epicyon-$filename" "$filename"
    sed -i 's|--font-size-header:.*|--font-size-header: 22px;|g' "$filename"    
    sed -i 's|--font-size:.*|--font-size: 45px;|g' "$filename"
fi

if [[ "$themeName" == 'purple'* ]]; then
    if [ -f config.json ]; then
	if grep -q '"theme"' config.json; then
	    sed -i 's|"theme".*|"theme": "purple",|g' config.json
	fi	
    fi
    filename=epicyon.css
    cp epicyon-profile.css "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #1f152d;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #1a142d;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #12152d;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #1f192d;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: #f98bb0;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: #3f2145;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #ff42a0;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #f93bb0;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: #f98bb0;|g" "$filename"
    sed -i "s|--button-selected:.*|--button-selected: #c042a0;|g" "$filename"
    sed -i "s|--button-background:.*|--button-background: #ff42a0;|g" "$filename"
    sed -i "s|--button-text:.*|--button-text: white;|g" "$filename"
    sed -i "s|background-color: #554;|background-color: #ff42a0;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: #1f152d;|g" "$filename"
    filename=calendar.css
    cp "epicyon-$filename" "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #1f152d;|g" "$filename"
    sed -i "s|--calendar-bg-color:.*|--calendar-bg-color: #eee;|g" "$filename"
    sed -i "s|--lines-color:.*|--lines-color: #ff42a0;|g" "$filename"
    sed -i "s|--day-number:.*|--day-number: #3f2145;|g" "$filename"
    sed -i "s|--day-number2:.*|--day-number2: #1f152d;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: #3f2145;|g" "$filename"
    sed -i "s|--place-color:.*|--place-color: black;|g" "$filename"
    sed -i "s|--event-color:.*|--event-color: #282c37;|g" "$filename"
    sed -i "s|--today-foreground:.*|--today-foreground: white;|g" "$filename"
    sed -i "s|--today-circle:.*|--today-circle: red;|g" "$filename"
    sed -i "s|--event-background:.*|--event-background: yellow;|g" "$filename"
    sed -i "s|--event-foreground:.*|--event-foreground: white;|g" "$filename"
    sed -i "s|--title-text:.*|--title-text: white;|g" "$filename"
    sed -i "s|--title-background:.*|--title-background: #ff42a0;|g" "$filename"
    filename=login.css
    cp "epicyon-$filename" "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #1f152d;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #1a142d;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #12152d;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #1f192d;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: #f98bb0;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: #3f2145;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #ff42a0;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #f93bb0;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: #f98bb0;|g" "$filename"
    sed -i "s|--button-selected:.*|--button-selected: #c042a0;|g" "$filename"
    sed -i "s|--button-background:.*|--button-background: #ff42a0;|g" "$filename"
    sed -i "s|--button-text:.*|--button-text: white;|g" "$filename"
    sed -i "s|background-color: #554;|background-color: #ff42a0;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: #1f152d;|g" "$filename"
    filename=follow.css
    cp "epicyon-$filename" "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #1f152d;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #1a142d;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #12152d;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #1f192d;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: #f98bb0;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: #3f2145;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #ff42a0;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #f93bb0;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: #f98bb0;|g" "$filename"
    sed -i "s|--button-selected:.*|--button-selected: #c042a0;|g" "$filename"
    sed -i "s|--button-background:.*|--button-background: #ff42a0;|g" "$filename"
    sed -i "s|--button-text:.*|--button-text: white;|g" "$filename"
    sed -i "s|background-color: #554;|background-color: #ff42a0;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: #1f152d;|g" "$filename"
    filename=suspended.css
    cp "epicyon-$filename" "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #1f152d;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #1a142d;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #12152d;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #1f192d;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: #f98bb0;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: #3f2145;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #ff42a0;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #f93bb0;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: #f98bb0;|g" "$filename"
    sed -i "s|--button-selected:.*|--button-selected: #c042a0;|g" "$filename"
    sed -i "s|--button-background:.*|--button-background: #ff42a0;|g" "$filename"
    sed -i "s|--button-text:.*|--button-text: white;|g" "$filename"
    sed -i "s|background-color: #554;|background-color: #ff42a0;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: #1f152d;|g" "$filename"
fi

if [[ "$themeName" == 'hack'* || "$themeName" == 'hax'* ]]; then
    if [ -f config.json ]; then
	if grep -q '"theme"' config.json; then
	    sed -i 's|"theme".*|"theme": "hacker",|g' config.json
	fi	
    fi
    filename=epicyon.css
    cp epicyon-profile.css "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: black;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #030202;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #050202;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #1f192d;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: green;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: darkgreen;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #266020;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #3c8234;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: green;|g" "$filename"
    sed -i "s|--button-selected:.*|--button-selected: #063200;|g" "$filename"
    sed -i "s|--button-background:.*|--button-background: #062200;|g" "$filename"
    sed -i "s|--button-text:.*|--button-text: green;|g" "$filename"
    sed -i "s|--button-corner-radius:.*|--button-corner-radius: 4px;|g" "$filename"
    sed -i "s|--timeline-border-radius:.*|--timeline-border-radius: 4px;|g" "$filename"
    sed -i "s|font-family:.*|font-family: Courier;|g" "$filename"
    sed -i "s|background-color: #554;|background-color: #062200;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: green;|g" "$filename"
    filename=calendar.css
    cp "epicyon-$filename" "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: black;|g" "$filename"
    sed -i "s|--calendar-bg-color:.*|--calendar-bg-color: black;|g" "$filename"
    sed -i "s|--lines-color:.*|--lines-color: green;|g" "$filename"
    sed -i "s|--day-number:.*|--day-number: green;|g" "$filename"
    sed -i "s|--day-number2:.*|--day-number2: darkgreen;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: darkgreen;|g" "$filename"
    sed -i "s|--place-color:.*|--place-color: green;|g" "$filename"
    sed -i "s|--event-color:.*|--event-color: green;|g" "$filename"
    sed -i "s|--today-foreground:.*|--today-foreground: white;|g" "$filename"
    sed -i "s|--today-circle:.*|--today-circle: red;|g" "$filename"
    sed -i "s|--event-background:.*|--event-background: lightgreen;|g" "$filename"
    sed -i "s|--event-foreground:.*|--event-foreground: black;|g" "$filename"
    sed -i "s|--title-text:.*|--title-text: black;|g" "$filename"
    sed -i "s|--title-background:.*|--title-background: darkgreen;|g" "$filename"
    sed -i "s|font-family:.*|font-family: 'Courier';|g" "$filename"
    filename=login.css
    cp "epicyon-$filename" "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: black;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #030202;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #050202;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #1f192d;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: green;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: darkgreen;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #266020;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #3c8234;|g" "$filename"
    sed -i "s|--button-selected:.*|--button-selected: #063200;|g" "$filename"
    sed -i "s|--button-background:.*|--button-background: #062200;|g" "$filename"
    sed -i "s|--button-text:.*|--button-text: green;|g" "$filename"
    sed -i "s|--button-corner-radius:.*|--button-corner-radius: 4px;|g" "$filename"
    sed -i "s|--timeline-border-radius:.*|--timeline-border-radius: 4px;|g" "$filename"
    sed -i "s|--form-border-radius:.*|--form-border-radius: 4px;|g" "$filename"
    sed -i "s|font-family:.*|font-family: Courier;|g" "$filename"
    sed -i "s|background-color: #554;|background-color: #062200;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: green;|g" "$filename"
    filename=follow.css
    cp "epicyon-$filename" "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: black;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #030202;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #050202;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #1f192d;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: green;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: darkgreen;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #266020;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #3c8234;|g" "$filename"
    sed -i "s|--button-selected:.*|--button-selected: #063200;|g" "$filename"
    sed -i "s|--button-background:.*|--button-background: #062200;|g" "$filename"
    sed -i "s|--button-text:.*|--button-text: green;|g" "$filename"
    sed -i "s|--button-corner-radius:.*|--button-corner-radius: 4px;|g" "$filename"
    sed -i "s|--timeline-border-radius:.*|--timeline-border-radius: 4px;|g" "$filename"
    sed -i "s|font-family:.*|font-family: Courier;|g" "$filename"
    sed -i "s|background-color: #554;|background-color: #062200;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: green;|g" "$filename"
    filename=suspended.css
    cp "epicyon-$filename" "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: black;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #030202;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #050202;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #1f192d;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: green;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: darkgreen;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #266020;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #3c8234;|g" "$filename"
    sed -i "s|--button-selected:.*|--button-selected: #063200;|g" "$filename"
    sed -i "s|--button-background:.*|--button-background: #062200;|g" "$filename"
    sed -i "s|--button-text:.*|--button-text: green;|g" "$filename"
    sed -i "s|--button-corner-radius:.*|--button-corner-radius: 4px;|g" "$filename"
    sed -i "s|--timeline-border-radius:.*|--timeline-border-radius: 4px;|g" "$filename"
    sed -i "s|font-family:.*|font-family: Courier;|g" "$filename"
    sed -i "s|background-color: #554;|background-color: #062200;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: green;|g" "$filename"
fi

if [[ "$themeName" == 'light'* ]]; then
    if [ -f config.json ]; then
	if grep -q '"theme"' config.json; then
	    sed -i 's|"theme".*|"theme": "light",|g' config.json
	fi	
    fi
    filename=epicyon.css
    cp epicyon-profile.css "$filename"
    sed -i 's|rgba(0, 0, 0, 0.5)|rgba(0, 0, 0, 0.0)|g' "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #e6ebf0;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #e0dbf0;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #e3dbf0;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #ebebf0;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: #2d2c37;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: #c0cdd9;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #2a2c37;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #232c37;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: #555;|g" "$filename"
    sed -i "s|--text-entry-foreground:.*|--text-entry-foreground: #111;|g" "$filename"
    sed -i "s|--text-entry-background:.*|--text-entry-background: white;|g" "$filename"
    sed -i "s|--font-color-header:.*|--font-color-header: black;|g" "$filename"
    sed -i "s|--dropdown-bg-color:.*|--dropdown-bg-color: white;|g" "$filename"    
    sed -i "s|--dropdown-bg-color-hover:.*|--dropdown-bg-color-hover: lightgrey;|g" "$filename"    
    sed -i "s|background-color: #554;|background-color: white;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: black;|g" "$filename"
    filename=calendar.css
    cp "epicyon-$filename" "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #e6ebf0;|g" "$filename"
    sed -i "s|--calendar-bg-color:.*|--calendar-bg-color: #e6ebf0;|g" "$filename"
    sed -i "s|--lines-color:.*|--lines-color: darkblue;|g" "$filename"
    sed -i "s|--day-number:.*|--day-number: black;|g" "$filename"
    sed -i "s|--day-number2:.*|--day-number2: #282c37;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: black;|g" "$filename"
    sed -i "s|--place-color:.*|--place-color: black;|g" "$filename"
    sed -i "s|--event-color:.*|--event-color: #282c37;|g" "$filename"
    sed -i "s|--today-foreground:.*|--today-foreground: white;|g" "$filename"
    sed -i "s|--today-circle:.*|--today-circle: red;|g" "$filename"
    sed -i "s|--event-background:.*|--event-background: lightblue;|g" "$filename"
    sed -i "s|--event-foreground:.*|--event-foreground: white;|g" "$filename"
    sed -i "s|--title-text:.*|--title-text: #282c37;|g" "$filename"
    sed -i "s|--title-background:.*|--title-background: #ccc;|g" "$filename"
    filename=login.css
    cp "epicyon-$filename" "$filename"
    sed -i 's|rgba(0, 0, 0, 0.5)|rgba(0, 0, 0, 0.0)|g' "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #e6ebf0;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #e0dbf0;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #e3dbf0;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #ebebf0;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: #2d2c37;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: #c0cdd9;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #2a2c37;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #232c37;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: #555;|g" "$filename"
    sed -i "s|--text-entry-foreground:.*|--text-entry-foreground: #111;|g" "$filename"
    sed -i "s|--text-entry-background:.*|--text-entry-background: white;|g" "$filename"
    sed -i "s|--font-color-header:.*|--font-color-header: black;|g" "$filename"
    sed -i "s|--dropdown-bg-color:.*|--dropdown-bg-color: white;|g" "$filename"    
    sed -i "s|--dropdown-bg-color-hover:.*|--dropdown-bg-color-hover: lightgrey;|g" "$filename"    
    sed -i "s|background-color: #554;|background-color: white;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: black;|g" "$filename"
    filename=follow.css
    cp "epicyon-$filename" "$filename"
    sed -i 's|rgba(0, 0, 0, 0.5)|rgba(0, 0, 0, 0.0)|g' "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #e6ebf0;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #e0dbf0;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #e3dbf0;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #ebebf0;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: #2d2c37;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: #c0cdd9;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #2a2c37;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #232c37;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: #555;|g" "$filename"
    sed -i "s|--text-entry-foreground:.*|--text-entry-foreground: #111;|g" "$filename"
    sed -i "s|--text-entry-background:.*|--text-entry-background: white;|g" "$filename"
    sed -i "s|--font-color-header:.*|--font-color-header: black;|g" "$filename"
    sed -i "s|--dropdown-bg-color:.*|--dropdown-bg-color: white;|g" "$filename"    
    sed -i "s|--dropdown-bg-color-hover:.*|--dropdown-bg-color-hover: lightgrey;|g" "$filename"    
    sed -i "s|background-color: #554;|background-color: white;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: black;|g" "$filename"
    filename=suspended.css
    cp "epicyon-$filename" "$filename"
    sed -i 's|rgba(0, 0, 0, 0.5)|rgba(0, 0, 0, 0.0)|g' "$filename"
    sed -i "s|--main-bg-color:.*|--main-bg-color: #e6ebf0;|g" "$filename"
    sed -i "s|--main-bg-color-reply:.*|--main-bg-color-reply: #e0dbf0;|g" "$filename"
    sed -i "s|--main-bg-color-report:.*|--main-bg-color-report: #e3dbf0;|g" "$filename"
    sed -i "s|--main-header-color-roles:.*|--main-header-color-roles: #ebebf0;|g" "$filename"
    sed -i "s|--main-fg-color:.*|--main-fg-color: #2d2c37;|g" "$filename"
    sed -i "s|--border-color:.*|--border-color: #c0cdd9;|g" "$filename"
    sed -i "s|--main-link-color:.*|--main-link-color: #2a2c37;|g" "$filename"
    sed -i "s|--main-visited-color:.*|--main-visited-color: #232c37;|g" "$filename"
    sed -i "s|--time-color:.*|--time-color: #555;|g" "$filename"
    sed -i "s|--text-entry-foreground:.*|--text-entry-foreground: #111;|g" "$filename"
    sed -i "s|--text-entry-background:.*|--text-entry-background: white;|g" "$filename"
    sed -i "s|--font-color-header:.*|--font-color-header: black;|g" "$filename"
    sed -i "s|--dropdown-bg-color:.*|--dropdown-bg-color: white;|g" "$filename"    
    sed -i "s|--dropdown-bg-color-hover:.*|--dropdown-bg-color-hover: lightgrey;|g" "$filename"    
    sed -i "s|background-color: #554;|background-color: white;|g" "$filename"
    sed -i "s|color: #FFFFFE;|color: black;|g" "$filename"
fi
